module.exports = {
    name: 'prune',
    aliases: ["clean", "purge", "cls"],
	description: 'Prune up to 99 messages.',
	execute(message, args) {
		const amount = parseInt(args[0]) + 1;

		if (isNaN(amount)) {
			return message.reply('Cela ne semble pas être un nombre valide.');
		} else if (amount <= 1 || amount > 100) {
			return message.reply('Tu dois entrer un nombre compris entre 1 et 99.');
		}

		message.channel.bulkDelete(amount, true).catch(err => {
			console.error(err);
			message.channel.send('Une erreur s\'est produite lors de la tentative de supprésion des messages dans ce canal!');
		});
	},
};